# Diagonal Difference

## Problem

Given a square matrix, calculate the absolute difference between the sums of its diagonals.

For example, the square matrix `arr` is shown below:

```s
1 2 3
4 5 6
7 8 9
```

The left-to-right diagonal 1 + 5 + 9 = 15. The right to left diagonal 3 + 5 + 9 = 17. Their absolute difference is |15-17| = 2.

### Function Description

It must return an integer representing the absolute diagonal difference.

`diagonalDifference` has the following parameter(s):

- `arr`: an array of array which contains integers representing the matrix.

### Input Format

The first line contains a single integer, `n`, the number of rows and columns in the matrix `arr`
Each of the next `n` lines describes a row, `arr[i]`, and consists of `n` space-separated integers `arr[i][j]`.

### Constraints

- -100 <= `ar[i][j]` <= 100

### Output Format

Print the absolute difference between the sums of the matrix's two diagonals as a single integer.

#### Sample Input

```s
3
11 2 4
4 5 6
10 8 -12
```

#### Sample Output

```s
15
```

#### Explanation

The primary diagonal is:

```s
11
   5
     -12
```

Sum across the primary diagonal: 11 + 5 - 12 = 4

The secondary diagonal is:

```s
     4
   5
10
```

Sum across the secondary diagonal: 4 + 5 + 10 = 19

Difference: |4 - 19| = 15

Note: |x| is the [absolute value](https://www.mathsisfun.com/numbers/absolute-value.html) of x

## Source

[HackerRank - Diagonal Difference](https://www.hackerrank.com/challenges/diagonal-difference/problem)