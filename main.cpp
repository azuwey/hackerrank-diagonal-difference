/**
 * Author: Zarandi David (Azuwey)
 * Date: 02/20/2019
 * Source: HackerRank - https://www.hackerrank.com/challenges/diagonal-difference/problem
 **/
#include <bits/stdc++.h>

using namespace std;

int diagonalDifference(vector<vector<int>> arr) {
  int ld = 0;
  int rd = 0;

  for (auto it = arr.begin(); it != arr.end(); it++) {
    int i = distance(arr.begin(), it);
    int j = abs(distance(arr.end(), it)) - 1;

    // Left to right
    try {
      ld += arr[i][i];
    } catch (const out_of_range &e) {
      ld += 0;
    }

    // Right to Left
    try {
      rd += arr[i][j];
    } catch (const out_of_range &e) {
      rd += 0;
    }
  }

  return abs(ld - rd);
}

int main() {
  ofstream fout(getenv("OUTPUT_PATH"));

  int n;
  cin >> n;
  cin.ignore(numeric_limits<streamsize>::max(), '\n');

  vector<vector<int>> arr(n);
  for (int i = 0; i < n; i++) {
    arr[i].resize(n);

    for (int j = 0; j < n; j++) {
      cin >> arr[i][j];
    }

    cin.ignore(numeric_limits<streamsize>::max(), '\n');
  }

  int result = diagonalDifference(arr);

  fout << result << "\n";

  fout.close();

  return 0;
}
